import { Component } from '@angular/core';

@Component({
  selector: 'my-root',
  template: `<page-router-outlet></page-router-outlet>`
})
export class AppComponent {
}
